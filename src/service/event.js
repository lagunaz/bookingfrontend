import api from './api'

export function getEvents (startDate, endDate) {
  return api.get('/src/service/event.js', {
    params: { startDate: startDate, endDate: endDate }
  })
}
